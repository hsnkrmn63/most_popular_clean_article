import 'dart:convert';
import 'package:most_popular_clean_article/feature/_exports.dart';
import 'package:flutter_test/flutter_test.dart';
import '../../../../fixtures/fixture_reader.dart';

void main() {
  final mostPopularsObject = MostPopulars(
      copyright:
          "Copyright (c) 2023 The New York Times Company.  All Rights Reserved.",
      status: "OK",
      numResults: 1,
      results: [
        MostPopular(
            uri: "nyt://article/00eee1ee-5934-5d7b-a28a-3b1f70bf9e86",
            url:
                "https://www.nytimes.com/2023/06/03/opinion/sierra-leone-progress-health-children.html",
            id: 100000008931916,
            assetId: 100000008931916,
            source: "New York Times",
            publishedDate: DateTime.tryParse("2023-06-03"),
            updated: DateTime.tryParse("2023-06-04 12:21:50"),
            section: "Opinion",
            subsection: "",
            nytdsection: "opinion",
            adxKeywords:
                "Pregnancy and Childbirth;Maternal Mortality;Women and Girls;Developing Countries;Poverty;Children and Childhood;Infant Mortality;Humanitarian Aid;Makeni (Sierra Leone);Sierra Leone",
            column: null,
            byline: "By Nicholas Kristof",
            type: "Article",
            title:
                "This May Be the Most Important Thing Happening in the World Today",
            resultAbstract:
                "Fewer children are starving, fewer moms are dying and terrible diseases are retreating.",
            desFacet: [
              "Pregnancy and Childbirth",
              "Maternal Mortality",
              "Women and Girls",
              "Developing Countries",
              "Poverty",
              "Children and Childhood",
              "Infant Mortality",
              "Humanitarian Aid"
            ],
            orgFacet: [],
            perFacet: [],
            geoFacet: ["Makeni (Sierra Leone)", "Sierra Leone"],
            etaId: 0,
            media: [
              Media(
                type: "image",
                subtype: "photo",
                caption:
                    "Yeabu Kargbo, 19, rests post-delivery at a rural health center in northern Sierra Leone.",
                copyright:
                    "Photographs by Malin Fezehai for The New York Times",
                approvedForSyndication: 1,
                mediaMetadata: [
                  MediaMetaData(
                      url:
                          "https://static01.nyt.com/images/2023/06/03/multimedia/03Kristof-1-wcmt/03Kristof-1-wcmt-thumbStandard-v2.jpg",
                      format: "Standard Thumbnail",
                      height: 75,
                      width: 75)
                ],
              )
            ])
      ]);

  test(
    'should be a subclass of MostPopulars entity',
    () async {
      // assert
      expect(mostPopularsObject, isA<MostPopulars>());
    },
  );

  group('fromJson', () {
    test(
      'should return a valid model when the JSON ',
      () async {
        // arrange
        final Map<String, dynamic> jsonMap =
            json.decode(fixture('most_populars.json'));
        // act
        final result = MostPopulars.fromMap(jsonMap);
        // assert
        //todo equtable will be made
        expect(result.runtimeType, mostPopularsObject.runtimeType);
      },
    );
  });
}
