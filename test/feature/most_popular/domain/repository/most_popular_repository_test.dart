import 'package:most_popular_clean_article/feature/_exports.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockMostPopularRepository extends Mock implements MostPopularRepository {}

void main() {
  late MockMostPopularRepository repository = MockMostPopularRepository();
  late MostPopularsUsecase mockMostPopularsUsecase =
      MostPopularsUsecase(repository);

  setUp(() {
    repository = MockMostPopularRepository();
    mockMostPopularsUsecase = MostPopularsUsecase(repository);
  });
}
