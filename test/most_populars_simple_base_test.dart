import 'dart:convert';

import 'package:most_popular_clean_article/feature/_exports.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'fixtures/fixture_reader.dart';
import 'most_populars_simple_base_test.mocks.dart';

Future<MostPopulars> fetchAlbum(http.Client client) async {
  final response = await client.get(Uri.parse(
      'https://api.nytimes.com/svc/mostpopular/v2/viewed/7.json?api-key=A1rRzLUpBjzO1s0xEZXgLQGilBoaPF4G'));

  if (response.statusCode == 200) {
    return MostPopulars.fromMap(jsonDecode(response.body));
  } else {
    throw Exception('Failed to load');
  }
}

@GenerateMocks([http.Client])
void main() {
  group('fetch', () {
    test('returns an MostPopulars if the http call completes successfully',
        () async {
      final client = MockClient();

      when(client.get(Uri.parse(
              'https://api.nytimes.com/svc/mostpopular/v2/viewed/7.json?api-key=A1rRzLUpBjzO1s0xEZXgLQGilBoaPF4G')))
          .thenAnswer(
              (_) async => http.Response(fixture('most_populars.json'), 200));

      expect(await fetchAlbum(client), isA<MostPopulars>());
    });

    test('throws an exception if the http call completes with an error', () {
      final client = MockClient();

      when(client.get(Uri.parse(
              'https://api.nytimes.com/svc/mostpopular/v2/viewed/7.json?api-key=A1rRzLUpBjzO1s0xEZXgLQGilBoaPF4G')))
          .thenAnswer((_) async => http.Response('Not Found', 404));

      expect(fetchAlbum(client), throwsException);
    });
  });
}
