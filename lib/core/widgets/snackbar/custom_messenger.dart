import '../../_core_exports.dart';

class CustomMessenger extends StatelessWidget {
  const CustomMessenger(
      {final Key? key, required this.state, required this.content})
      : super(key: key);
  final CustomMessengerState state;
  final String content;
  @override
  Widget build(final BuildContext context) => Align(
        alignment: Alignment.topCenter,
        child: Material(
          borderRadius: BorderRadius.circular(5.r),
          child: Container(
            padding:
                const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.r),
              color: state.getColor(),
            ),
            child: Row(
              children: [
                FaIcon(
                  state.getIconData(),
                  color: AppColors.saltWhite,
                  size: 20.0,
                ),
                SizedBox(
                  width: 10.0.w,
                ),
                Expanded(
                  child: Text(
                    content,
                    style: const TextStyle(
                      color: AppColors.saltWhite,
                    ),
                    maxLines: 6,
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
