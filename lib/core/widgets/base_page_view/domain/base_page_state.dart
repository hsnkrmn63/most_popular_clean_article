enum BasePageState {
  loading,
  error,
  succes,
  notallwed,
}

extension BasePageStateExtenstion on BasePageState {
  bool getStatu() {
    switch (this) {
      case BasePageState.loading:
        return true;
      case BasePageState.error:
        return false;

      case BasePageState.succes:
        return false;

      case BasePageState.notallwed:
        return false;
    }
  }
}
