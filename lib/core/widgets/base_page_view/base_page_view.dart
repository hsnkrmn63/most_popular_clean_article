import '../../_core_exports.dart';

class BasePageView extends StatelessWidget {
  const BasePageView({
    required this.basePageState,
    this.errorWidget,
    this.loadingWidget,
    required this.successWidget,
    required this.refreshFunction,
    this.notAllowedToSeeWidget,
    this.flexibleWidget,
    this.isHaveShimmer = false,
    Key? key,
  }) : super(key: key);

  final BasePageState basePageState;

  /// BasePageState.ERROR durumunda gösterilecek widget
  final Widget? errorWidget;

  /// BasePageState.LOADING durumunda gösterilecek widget
  final Widget? loadingWidget;

  /// BasePageState.NOTALLOWED durumunda gösterilecek widget
  final Widget? notAllowedToSeeWidget;

  /// loading, hasError == false ve isAllowedToSee == true olduğu durumda gösterilecek widget
  final Widget successWidget;

  /// listenin üstünde yer alacak widget. Search bar için tasarlandı.
  final Widget? flexibleWidget;

  final Future<void> Function() refreshFunction;

  /// Sliver List kullanılacağı durumlarda true olmalıdır.
  final bool? isHaveShimmer;

  @override
  Widget build(final BuildContext context) {
    switch (basePageState) {
      case BasePageState.error:
        return errorWidget!;
      case BasePageState.notallwed:
        return notAllowedToSeeWidget!;
      case BasePageState.succes:
      case BasePageState.loading:
        return GestureDetector(
          onTap: () {
            removeFocus(context);
          },
          child: Stack(
            children: [
              RefreshIndicator(
                onRefresh: refreshFunction,
                child: Column(
                  children: [
                    if (flexibleWidget != null) flexibleWidget!,
                    //shimmer olacak ise sayfa shimmer olarak tasarlanacak
                    Expanded(
                      child: (isHaveShimmer! &&
                              basePageState == BasePageState.loading)
                          ? loadingWidget ??
                              Container(
                                  height: double.infinity,
                                  width: double.infinity,
                                  color: AppColors.saltWhite.withOpacity(.1),
                                  alignment: Alignment.center,
                                  child: const CircularProgressIndicator())
                          : successWidget,
                    ),
                  ],
                ),
              ),
              if (!isHaveShimmer! && basePageState == BasePageState.loading)
                Align(
                  alignment: Alignment.center,
                  child: loadingWidget ??
                      Container(
                          height: double.infinity,
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: const CircularProgressIndicator()),
                )
            ],
          ),
        );
    }
  }
}
