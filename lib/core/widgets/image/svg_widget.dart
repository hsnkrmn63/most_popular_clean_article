import '../../_core_exports.dart';

class SvgWidget extends StatelessWidget {
  final String iconPath;
  final double height;
  final double width;
  final Color? iconColor;
  const SvgWidget(
      {final Key? key,
      required this.iconPath,
      required this.height,
      required this.width,
      this.iconColor})
      : super(key: key);

  @override
  Widget build(final BuildContext context) => SvgPicture.asset(
        iconPath,
        height: height.h,
        width: width.w,
        colorFilter: iconColor == null
            ? null
            : ColorFilter.mode(iconColor!, BlendMode.srcIn),
      );
}
