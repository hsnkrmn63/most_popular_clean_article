import '../_core_exports.dart';

extension ScreenSizeExtension on BuildContext {
  double get height => 812.h;
  double get width => 390.w;
}
