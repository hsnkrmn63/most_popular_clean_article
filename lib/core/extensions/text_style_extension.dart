import '../_core_exports.dart';

extension TextStyleExtensions on BuildContext {
  TextStyle get size20Bold500 => TextStyle(
        color: AppColors.saltWhite,
        fontWeight: FontWeight.w700,
        fontSize: 20.sp,
      );

  TextStyle get size14Bold600 => TextStyle(
        color: AppColors.saltBlack,
        fontWeight: FontWeight.w600,
        fontSize: 14.sp,
      );
  TextStyle get size14Bold400 => TextStyle(
        color: AppColors.grey,
        fontWeight: FontWeight.w400,
        fontSize: 14.sp,
      );
}
