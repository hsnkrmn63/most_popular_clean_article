import 'package:dartz/dartz.dart';

import '../_core_exports.dart';

abstract class Usecase<Type, Params> {
  Future<Either<Failure, Type>> call(final Params params);
}

class NoParams extends Equatable {
  const NoParams();
  @override
  List<Object> get props => [];
}
