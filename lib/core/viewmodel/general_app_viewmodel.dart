import '../_core_exports.dart';

///
/// uygulama içinde genel viewmodel controller olarak kullanılacaktır
///
class GeneralAppViewModel extends ChangeNotifier {
  GeneralAppViewModel();
  final RouteObserver<ModalRoute<void>> routeObserver =
      RouteObserver<ModalRoute<void>>();
// token bilgisi tutulur
  String? _accesToken;
  String? get accessToken => _accesToken;
  void setAccessToken(String value) {
    _accesToken = value;
    notifyListeners();
  }

//* overlay durum kontrolu
  OverlayEntry? overlayEntry;

// global buildcontext'i tutar
  late BuildContext _globalcontext;

  BuildContext get globalContext => _globalcontext;

  void setGlobalContext(BuildContext context) {
    _globalcontext = context;
    notifyListeners();
  }
}
