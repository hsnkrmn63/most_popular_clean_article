import '../_core_exports.dart';

class ConnectionViewModel extends ChangeNotifier {
  ConnectionViewModel() {
    //Create instance
    final ConnectionUtil connectionStatus = ConnectionUtil();
    //Initialize
    connectionStatus.initialize();
    //Listen for connection change
    connectionStatus.connectionChange.listen(connectionChanged);
  }

  // ConnectionUtil
  bool hasInterNetConnection = false;

  void connectionChanged(final dynamic hasConnection) {
    hasInterNetConnection = hasConnection;
    if (!hasInterNetConnection) {
      showNetworkErrorDialog();
    }
  }

  void showNetworkErrorDialog() {
    showCustomMessenger(
      CustomMessengerState.error,
      "İnternet bağlantısı yok.Lütfen internet bağlantınızı kontrol ediniz ",
    );
  }
}
