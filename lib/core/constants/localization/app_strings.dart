import '../../_core_exports.dart';

class AppStrings {
  final Locale _locale;
  Map<String, String> _localizedStrings = const {};
  static const LocalizationsDelegate<AppStrings> delegate =
      AppStringsDelegate();

  AppStrings(this._locale);

  static AppStrings of() => Localizations.of<AppStrings>(
          GlobalContextKey.instance.globalKey.currentContext!, AppStrings)
      as AppStrings;

  Future<void> loadStrings() async {
    //  await serviceLocator<SettingsLanguagePreferencesProvider>().init(deviceLocalLanguage: _locale);
    //final languageProv = serviceLocator<SettingsLanguagePreferencesProvider>();
    String jsonString;
    jsonString = await rootBundle
        .loadString("assets/langs/${_locale.languageCode}.json");

    final Map<String, dynamic> jsonMap = json.decode(jsonString);
    _localizedStrings = jsonMap.map(
      (final key, final value) => MapEntry(key,
          value.toString().isEmpty ? "ceviri yapılacak" : value.toString()),
    );
  }

  String? get nyTimesMostPopular => _localizedStrings['nyTimesMostPopular'];
  String? get detail => _localizedStrings['detail'];
  String? get timeOut => _localizedStrings['timeOut'];
  String? get newtorkConnectionControl =>
      _localizedStrings['newtorkConnectionControl'];
}
