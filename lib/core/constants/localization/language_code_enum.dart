enum LanguageCodeEnum {
  tr,
  en,
}

extension LanguageCodeExtension on LanguageCodeEnum {
  String languageCodeToString() {
    switch (this) {
      case LanguageCodeEnum.tr:
        return "tr";
      case LanguageCodeEnum.en:
        return "en";
    }
  }

  LanguageCodeEnum stringToLanguageCode(final String value) {
    switch (value) {
      case "tr":
        return LanguageCodeEnum.tr;
      case "Türkçe":
        return LanguageCodeEnum.tr;
      case "en":
        return LanguageCodeEnum.en;
      case "English":
        return LanguageCodeEnum.en;
      default:
        return LanguageCodeEnum.tr;
    }
  }
}
