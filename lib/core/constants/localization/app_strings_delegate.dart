import '../../_core_exports.dart';

class AppStringsDelegate extends LocalizationsDelegate<AppStrings> {
  const AppStringsDelegate();

  @override
  bool isSupported(final Locale locale) => [
        'tr',
        'en',
      ].contains(locale.languageCode);

  @override
  Future<AppStrings> load(final Locale locale) async {
    final AppStrings appLocalization = AppStrings(locale);
    await appLocalization.loadStrings();
    return appLocalization;
  }

  @override
  bool shouldReload(final LocalizationsDelegate old) => true;
}
