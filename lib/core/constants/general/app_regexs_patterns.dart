class AppRegexpPatterns {
  ///Not 0123456789
  static const String notNumbers = r"[^0-9]";

  ///Not 0123456789.,
  static const String notNumbersDotOrComma = r"[^0-9.,]";

  ///0123456789.
  static const String numbersAndDot = r'[0-9.]';

  //special charachter

  static const String oneSpecialCharacter = r'^(?=.*[-+_!@#$%^&*., ?])';

  // uppercase
  static const String oneUpperCase = r'^(?=.*[A-Z])';
  //number
  static const String oneNumberCharacter = r'^(?=.*[0-9])';

  static const String is8Character = r'^.{8,} ';

  //email
  static const String mailRegex =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
}
