import '../../_core_exports.dart';

class AppColors {
  static const Color turquoise = Color.fromARGB(255, 6, 191, 173);
  static const Color saltWhite = Color.fromARGB(255, 248, 248, 254);
  static const Color saltBlack = Color.fromARGB(255, 5, 5, 5);
  static const Color lightGrey = Color.fromARGB(255, 224, 221, 221);
  static const Color grey = Color.fromARGB(255, 158, 158, 158);

  static const Color succesGreen = Color.fromARGB(255, 123, 214, 98);
  static const Color failRed = Color(0xFFDA281C);
}
