import 'package:most_popular_clean_article/feature/_exports.dart';
import 'package:provider/single_child_widget.dart';

import '../../_core_exports.dart';

class MainViewModels {
  /// Bu liste uygulama başlarken ayağa kalkması gereken providerlerı içerir.
  static final List<SingleChildWidget> mainViewModels = [
    ChangeNotifierProvider<MostPopularViewmModel>(
      create: (final _) => serviceLocator(),
    ),
    ChangeNotifierProvider<ConnectionViewModel>(
      create: (final _) => serviceLocator(),
    ),
  ];

  /// Bu metot uygulama başında ayağa kalkması gereken provider listesini döner
  static List<SingleChildWidget> getMainViewModels() => mainViewModels;
}
