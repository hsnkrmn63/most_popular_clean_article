import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';

class ConnectionUtil {
  static final ConnectionUtil _singleton = ConnectionUtil._internal();
  factory ConnectionUtil() => _singleton;
  ConnectionUtil._internal();

  bool hasConnection = false;
  //Connection değişimlerine abone oluyoruz.
  StreamController connectionChangeController = StreamController();
  //flutter_connectivity_plus
  final Connectivity _connectivity = Connectivity();
  void initialize() {
    _connectivity.onConnectivityChanged.listen(_connectionChange);
  }

  //connectivity listener
  void _connectionChange(final ConnectivityResult result) {
    hasInternetConnection();
  }

  Stream get connectionChange => connectionChangeController.stream;

  Future<bool> hasInternetConnection() async {
    final bool previousConnection = hasConnection;
    final connectivityResult = await Connectivity().checkConnectivity();
    // connected to wifi or mobile
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      hasConnection = true;
    }
    // no connection
    else {
      hasConnection = false;
    }
    // Connection değişirse tüm listenerlar update edilir.
    if (previousConnection != hasConnection) {
      connectionChangeController.add(hasConnection);
    }
    return hasConnection;
  }
}
