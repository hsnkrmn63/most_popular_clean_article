import 'package:get_it/get_it.dart';

import '../../feature/_exports.dart';
import '../_core_exports.dart';

final serviceLocator = GetIt.instance;

Future<void> init() async {
  //?Everything Provider
  //* Provider
  serviceLocator
      .registerLazySingleton<GeneralAppViewModel>(() => GeneralAppViewModel());
  //? PingPong Resource Url
  //* DebugModeOptions
  serviceLocator.registerLazySingleton<DebugModeOptions>(DebugModeOptions.new);
  //*Repo
  serviceLocator.registerLazySingleton<RemoteEndpoints>(RemoteEndpoints.new);
  //? Remote Data Source
  //* Repo
  serviceLocator.registerLazySingleton<IBaseRequestRepository>(
      () => BaseRequestRepoImpl());

  //*Usecase
  serviceLocator
      .registerLazySingleton(() => BaseRequestUsecase(serviceLocator()));

  //?ConnectionViewModel
  //* Provider
  serviceLocator
      .registerLazySingleton<ConnectionViewModel>(ConnectionViewModel.new);
  serviceLocator.registerLazySingleton(() => ConnectionUtil());

//? local storage
  serviceLocator.registerLazySingleton(() => const FlutterSecureStorage());

  //* Repo
  serviceLocator.registerLazySingleton<SecureStorageRepository>(
      () => SecureStorageRepositoryImpl(serviceLocator()));
  //* Use Case
  serviceLocator
      .registerLazySingleton(() => GetSecureDataFromKey(serviceLocator()));
  serviceLocator
      .registerLazySingleton(() => RemoveSecureDataFromKey(serviceLocator()));
  serviceLocator
      .registerLazySingleton(() => SaveSecureDataFromKey(serviceLocator()));

  serviceLocator.registerLazySingleton(
      () => GetSharedStorageDataFromKey(serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => RemoveSharedStorageDataFromKey(serviceLocator()));
  serviceLocator.registerLazySingleton(
      () => SaveSharedStorageDataFromKey(serviceLocator()));

  //?auth
  //*splash
  serviceLocator.registerLazySingleton<MostPopularViewmModel>(
      () => MostPopularViewmModel(serviceLocator()));
  serviceLocator
      .registerLazySingleton<MostPopularRepository>(() => MostPopularsRepoImpl(
            serviceLocator(),
          ));
  serviceLocator.registerLazySingleton<MostPopularsUsecase>(
      () => MostPopularsUsecase(serviceLocator()));
}
