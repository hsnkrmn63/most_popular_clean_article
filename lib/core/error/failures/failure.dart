import '../../_core_exports.dart';

abstract class Failure extends Equatable {
  const Failure([final List properties = const <dynamic>[]]);

  ///Metotların içerisinde gösterilecek olan snackbarın
  void snackBarMethod() {
    if (runtimeType == UnauthorizedFailure) {
      showCustomMessenger(
        CustomMessengerState.error,
        AppStrings.of().timeOut!,
      );
      Future.delayed(const Duration(milliseconds: 2), () {
        Go.to.pageAndRemoveUntil(AppRouters.mostPopularView);
      });
    }
    if (runtimeType != NoInternetConnectionFailure) {
      showCustomMessenger(
        CustomMessengerState.error,
        errorMessage(),
      );
    } else {
      showDialog(
        context: GlobalContextKey.instance.globalKey.currentContext!,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: Text(
              AppStrings.of().newtorkConnectionControl!,
              textAlign: TextAlign.center,
              style: context.size20Bold500.copyWith(color: AppColors.saltBlack),
            ),
            icon: Container(
              alignment: FractionalOffset.topRight,
              child: IconButton(
                onPressed: () {
                  Go.to.back();
                },
                icon: const Icon(Icons.clear),
              ),
            ),
            iconPadding: EdgeInsets.zero,
          );
        },
      );
    }
  }

  static Failure getFailure(String errorMessage) {
    ServiceMessageFailure.message = errorMessage;
    return ServiceMessageFailure();
  }
}

extension FailureExtension on Failure {
  String errorMessage() {
    return ServiceMessageFailure.message;
  }
}
