import '../../_core_exports.dart';

class UserNotFoundFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class TokenFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}
