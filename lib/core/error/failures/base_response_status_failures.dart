import '../../_core_exports.dart';

class BadRequestFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class UnauthorizedFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class ForbiddenFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class NotFoundFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class InternalFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class GatewayTimeOutFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class RequestCancelledFailure extends Failure {
  const RequestCancelledFailure(this.reason);
  final String reason;
  @override
  List<Object?> get props => [];
}

class NoInternetConnectionFailure extends Failure {
  @override
  List<Object?> get props => [NoInternetConnectionException];
}

class OperationCouldNotDoneFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class EducationFailuer extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class ExperienceLimitFailuer extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class ExperienceSameFailuer extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class CertificateSameFailuer extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class CertificateLimitFailuer extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class NameSurnameFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class VerificationCodeFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class SendSMSFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class NumberIsUsedFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}
