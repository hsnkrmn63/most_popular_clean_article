import '../../_core_exports.dart';

class NullPointerFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class ListEmptyFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}

class ServiceMessageFailure extends Failure {
  static String message = "";
  @override
  List<Object?> get props => throw UnimplementedError();
}

class PlatformExceptionFailure extends Failure {
  @override
  List<Object?> get props => [PlatformException];
}

class EmptyResultFailure extends Failure {
  @override
  List<Object?> get props => throw UnimplementedError();
}
