class WidgetNotFoundException<T> implements Exception {
  @override
  String toString() => "Class doesn't have state -> $T";
}
