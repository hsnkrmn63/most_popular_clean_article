import 'package:flutter/foundation.dart';

import '../_core_exports.dart';

class DebugModeOptions<T> {
  ApplicationEnvironment applicationEnvironment = ApplicationEnvironment.dev;

  static bool printEndpointToConsole = true && kDebugMode;
  static bool printRequestToConsole = true && kDebugMode;
  static bool printResponseToConsole = true && kDebugMode;
}
