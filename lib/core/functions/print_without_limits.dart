import '../_core_exports.dart';

void printWithoutLimits(final Object? object) {
  final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
  pattern
      .allMatches('$object')
      .forEach((final match) => debugPrint(match.group(0)));
}
