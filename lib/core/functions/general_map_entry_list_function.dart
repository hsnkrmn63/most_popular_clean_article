// static product listeleme islemi yapar
List<MapEntry<String, String>> generalMapEntryListFunction(
    {required List<String> title, required List<String> description}) {
  late List<MapEntry<String, String>> dataList = [];
  for (var i = 0; i < title.length; i++) {
    dataList.add(MapEntry(title[i], description[i]));
  }
  return dataList;
}
