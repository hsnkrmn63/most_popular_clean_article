import '../_core_exports.dart';

enum CustomMessengerState {
  succes,
  info,
  warning,
  error,
}

extension CustomMessengerHelper on CustomMessengerState {
  Color getColor() {
    switch (this) {
      case CustomMessengerState.succes:
        return AppColors.succesGreen;
      case CustomMessengerState.info:
        return Colors.blue;
      case CustomMessengerState.warning:
        return AppColors.failRed;
      case CustomMessengerState.error:
        return AppColors.failRed;
    }
  }

  IconData getIconData() {
    switch (this) {
      case CustomMessengerState.succes:
        return FontAwesomeIcons.circleCheck;
      case CustomMessengerState.info:
        return FontAwesomeIcons.circleCheck;
      case CustomMessengerState.warning:
        return FontAwesomeIcons.circleXmark;
      case CustomMessengerState.error:
        return FontAwesomeIcons.circleXmark;
    }
  }
}

///!Dikkat: [showCustomMessenger] kullanılırken 2 kere dikkat edilmeli çünkü tekrarlanabilen işlemler içinde,
///kullanıldığında birden fazla snackbar gösterilebiliyor. Örneğin;
///Tıklama 1: SnackBar1 - Gösteriliyor
///Ardaşık Tıklama 2-3-4-5-6 için SnackBar 2-3-4-5-6 listeye alınıyor ve
///snack bar duration'ına göre tüm snackbarların gösterilmesi xx saniye kadar sürebiliyor. Bilginize!!
///O yüzden kullanılan yere göre şu kod eklenebilir;
///ScaffoldMessenger.of(GlobalContextKey.instance.globalKey.currentContext!).clearSnackBars();
void showCustomMessenger(
    final CustomMessengerState messengerState, final String content,
    {final Duration? duration}) {
  ScaffoldMessenger.of(GlobalContextKey.instance.globalKey.currentContext!)
      .showSnackBar(
    SnackBar(
      duration: duration ?? const Duration(seconds: 3),
      margin: EdgeInsets.only(top: 20.h),
      backgroundColor: Colors.transparent,
      content: CustomMessenger(state: messengerState, content: content),
      behavior: SnackBarBehavior.floating,
      elevation: 0,
    ),
  );
}
