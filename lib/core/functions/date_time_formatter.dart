import 'package:intl/intl.dart';

String dateSimpleFormat(DateTime dateTime) =>
    DateFormat('dd-MM-yyyy').format(dateTime);
