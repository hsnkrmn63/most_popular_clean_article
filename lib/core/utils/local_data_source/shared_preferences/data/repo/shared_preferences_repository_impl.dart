import 'package:dartz/dartz.dart';
import '../../../../../_core_exports.dart';

class SecureStorageRepositoryImpl implements SecureStorageRepository {
  final FlutterSecureStorage flutterSecureStorage;
  SecureStorageRepositoryImpl(
    this.flutterSecureStorage,
  );

  @override
  Future<Either<Failure, String>> getSecureDataFromKey(
      final SecureStorageKeys key) async {
    final String? value =
        await flutterSecureStorage.read(key: key.sharedKeyStateToKeyValues());
    if (value != null) {
      return Right(value);
    } else {
      return Left(NullPointerFailure());
    }
  }

  @override
  Future<Either<Failure, void>> removeSecureDataFromKey(
      final SecureStorageKeys key) async {
    try {
      await flutterSecureStorage.delete(key: key.sharedKeyStateToKeyValues());
      return const Right(null);
    } on Failure catch (failure) {
      return Left(failure);
    }
  }

  @override
  Future<Either<Failure, void>> saveSecureDataFromKey(
      final SecureStorageKeys key, final String data) async {
    try {
      await flutterSecureStorage.write(
          key: key.sharedKeyStateToKeyValues(), value: data);
      return const Right(null);
    } on Failure catch (failure) {
      return Left(failure);
    }
  }

  @override
  Future<Either<Failure, void>> removeSharedStorageSecureDataFromKey(
      final SecureStorageKeys key) async {
    try {
      final prefs = await SharedPreferences.getInstance();

      await prefs.remove(key.sharedKeyStateToKeyValues());
      return const Right(null);
    } on Failure catch (failure) {
      return Left(failure);
    }
  }

  @override
  Future<Either<Failure, void>> saveSharedStorageSecureDataFromKey(
      final SecureStorageKeys key,
      dynamic data,
      final SharedStorageObjectTypeState sharedStorageObjectTypeState) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      if (sharedStorageObjectTypeState ==
              SharedStorageObjectTypeState.intType &&
          data.runtimeType == int) {
        await prefs.setInt(key.sharedKeyStateToKeyValues(), data);
      } else if (sharedStorageObjectTypeState ==
              SharedStorageObjectTypeState.boolType &&
          data.runtimeType == bool) {
        await prefs.setBool(key.sharedKeyStateToKeyValues(), data);
      } else if (sharedStorageObjectTypeState ==
              SharedStorageObjectTypeState.doubleType &&
          data.runtimeType == double) {
        await prefs.setDouble(key.sharedKeyStateToKeyValues(), data);
      } else if (sharedStorageObjectTypeState ==
              SharedStorageObjectTypeState.stringListType &&
          data.runtimeType == List<String>) {
        await prefs.setStringList(key.sharedKeyStateToKeyValues(), data);
      } else {
        await prefs.setString(key.sharedKeyStateToKeyValues(), data);
      }
      return const Right(null);
    } on Failure catch (failure) {
      return Left(failure);
    }
  }

  @override
  Future<Either<Failure, dynamic>> getSharedStorageDataFromKey(
      SecureStorageKeys key,
      SharedStorageObjectTypeState sharedStorageObjectTypeState) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      if (sharedStorageObjectTypeState ==
          SharedStorageObjectTypeState.intType) {
        return Right(prefs.getInt(key.sharedKeyStateToKeyValues()) ?? -1);
      } else if (sharedStorageObjectTypeState ==
          SharedStorageObjectTypeState.boolType) {
        return Right(prefs.getBool(key.sharedKeyStateToKeyValues()) ?? true);
      } else if (sharedStorageObjectTypeState ==
          SharedStorageObjectTypeState.doubleType) {
        return Right(prefs.getDouble(key.sharedKeyStateToKeyValues()) ?? 0.0);
      } else if (sharedStorageObjectTypeState ==
          SharedStorageObjectTypeState.stringListType) {
        return Right(
            prefs.getStringList(key.sharedKeyStateToKeyValues()) ?? []);
      } else {
        return Right(prefs.getString(key.sharedKeyStateToKeyValues()) ?? "");
      }
    } on Failure catch (failure) {
      return Left(failure);
    }
  }
}
