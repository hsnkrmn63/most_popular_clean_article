import '../../../../../_core_exports.dart';

class SharedPreferencesListModel<T> extends SharedPreferencesList<T>
    implements BaseModelRepo {
  SharedPreferencesListModel({
    required final List<T> list,
  }) : super(list: list);

  factory SharedPreferencesListModel.fromJson(final String str) {
    try {
      final Map<String, dynamic> decodedJson = json.decode(str);

      //*BaseModelRepo'dan implement edilmiş bir sınıf diyerek decode etmeye çalışıyoruz.
      //Eğer hata alırsak, catch içinde primitive bir tür olarak dönüş yapıyoruz.
      return SharedPreferencesListModel<T>(
        list: List<T>.from(
          decodedJson["list"].json.decode(str)["list"].map(
            (final element) {
              return json.decode(element);
            },
          ),
        ),
      );
    } catch (e) {
      return SharedPreferencesListModel<T>(
        list: List<T>.from(
          json.decode(str)["list"].map(
            (final element) {
              return element;
            },
          ),
        ),
      );
    }
  }

  ///Listede verilen parametrelere göre liste elemanı arayan ve silen, sildiği eleman sayısı kadar da sonuç döndüren fonksiyondur.
  ///[justDeleteFirstMatchedElement] true ise sadece ilk eşleşen elamanı,
  ///false ise tüm eşleşen elemanları siler.
  int deleteElementFromList({
    required final bool Function(T element) elementFounderFunction,
    final bool justDeleteFirstMatchedElement = true,
  }) {
    int deletedElementCount = 0;

    for (int i = 0; i < list.length; i++) {
      if (elementFounderFunction(list[i])) {
        list.removeAt(i);
        deletedElementCount = deletedElementCount + 1;
        if (justDeleteFirstMatchedElement) {
          break;
        }
      }
    }

    return deletedElementCount;
  }

  String toJson() => json.encode(toMap());

  @override
  Map<String, dynamic> toMap() => {
        "list": List<T>.from(
          list.map(
            (final element) {
              ///Eğer ki liste elemanı BaseModelRepodan implement edilmiş bir sınıf ise kaydederken Map şeklinde kaydediyoruz.
              if (element is BaseModelRepo) {
                return json.encode(element.toMap());
              }
              return element;
            },
          ),
        ),
      };
}
