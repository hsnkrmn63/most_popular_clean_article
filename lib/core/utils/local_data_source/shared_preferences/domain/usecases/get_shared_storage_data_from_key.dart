import 'package:dartz/dartz.dart';

import '../../../../../_core_exports.dart';

class GetSharedStorageDataFromKey
    implements Usecase<dynamic, SecureStorageKeyParams> {
  SecureStorageRepository repository;

  GetSharedStorageDataFromKey(this.repository);

  @override
  Future<Either<Failure, dynamic>> call(final params) async {
    return repository.getSharedStorageDataFromKey(
        params.key, params.sharedStorageObjectTypeState!);
  }
}
