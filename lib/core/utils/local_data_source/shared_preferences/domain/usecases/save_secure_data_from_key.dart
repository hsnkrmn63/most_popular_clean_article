import 'package:dartz/dartz.dart';

import '../../../../../_core_exports.dart';

class SaveSecureDataFromKey
    implements Usecase<void, SecureStorageKeyWithValueParams> {
  final SecureStorageRepository repository;

  SaveSecureDataFromKey(this.repository);

  @override
  Future<Either<Failure, void>> call(final params) async {
    return repository.saveSecureDataFromKey(params.key, params.value);
  }
}
