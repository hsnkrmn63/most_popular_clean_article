import 'package:dartz/dartz.dart';

import '../../../../../_core_exports.dart';

class RemoveSecureDataFromKey implements Usecase<void, SecureStorageKeyParams> {
  final SecureStorageRepository repository;

  RemoveSecureDataFromKey(this.repository);

  @override
  Future<Either<Failure, void>> call(final params) async {
    return repository.removeSecureDataFromKey(params.key);
  }
}
