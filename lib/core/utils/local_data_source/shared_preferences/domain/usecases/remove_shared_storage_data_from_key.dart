import 'package:dartz/dartz.dart';

import '../../../../../_core_exports.dart';

class RemoveSharedStorageDataFromKey
    implements Usecase<void, SecureStorageKeyParams> {
  final SecureStorageRepository repository;

  RemoveSharedStorageDataFromKey(this.repository);

  @override
  Future<Either<Failure, void>> call(final params) async {
    return repository.removeSharedStorageSecureDataFromKey(params.key);
  }
}
