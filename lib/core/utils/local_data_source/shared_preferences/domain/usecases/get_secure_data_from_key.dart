import 'package:dartz/dartz.dart';

import '../../../../../_core_exports.dart';

class GetSecureDataFromKey implements Usecase<String, SecureStorageKeyParams> {
  SecureStorageRepository repository;

  GetSecureDataFromKey(this.repository);

  @override
  Future<Either<Failure, String>> call(final params) async {
    return repository.getSecureDataFromKey(params.key);
  }
}
