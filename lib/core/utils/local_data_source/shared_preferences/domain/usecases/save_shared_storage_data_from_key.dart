import 'package:dartz/dartz.dart';

import '../../../../../_core_exports.dart';

class SaveSharedStorageDataFromKey
    implements Usecase<void, SharedStorageStorageKeyWithValueParams> {
  final SecureStorageRepository repository;

  SaveSharedStorageDataFromKey(this.repository);

  @override
  Future<Either<Failure, void>> call(final params) async {
    return repository.saveSharedStorageSecureDataFromKey(
        params.key, params.value, params.sharedStorageObjectTypeState!);
  }
}
