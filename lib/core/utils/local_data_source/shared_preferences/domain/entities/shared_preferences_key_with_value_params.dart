import '../../../../../_core_exports.dart';

class SecureStorageKeyWithValueParams {
  final SecureStorageKeys key;
  final String value;
  SecureStorageKeyWithValueParams({
    required this.key,
    required this.value,
  });
}
