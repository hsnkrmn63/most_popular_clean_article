class SharedPreferencesList<T> {
  final List<T> list;
  SharedPreferencesList({required this.list});
}
