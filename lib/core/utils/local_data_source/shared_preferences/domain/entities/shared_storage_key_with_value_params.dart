import '../../../../../_core_exports.dart';

class SharedStorageStorageKeyWithValueParams {
  final SecureStorageKeys key;
  dynamic value;
  final SharedStorageObjectTypeState? sharedStorageObjectTypeState;
  SharedStorageStorageKeyWithValueParams(
      {required this.key,
      required this.value,
      required this.sharedStorageObjectTypeState});
}
