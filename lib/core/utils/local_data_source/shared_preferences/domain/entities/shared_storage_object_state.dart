enum SharedStorageObjectTypeState {
  intType,
  boolType,
  doubleType,
  stringType,
  stringListType,
}
