enum SecureStorageKeys {
  languageSettings,
  cacheUserInfo,
  firstApp,
  cacheUserName
}

extension SecureStorageKeysExtension on SecureStorageKeys {
  String sharedKeyStateToKeyValues() {
    switch (this) {
      case SecureStorageKeys.languageSettings:
        return "LANGUAGE_SETTINGS";
      case SecureStorageKeys.cacheUserInfo:
        return "CACHE_USER_INFO";
      case SecureStorageKeys.firstApp:
        return "FIRST_APP";
      case SecureStorageKeys.cacheUserName:
        return "CACHE_USER_NAME";
    }
  }
}
