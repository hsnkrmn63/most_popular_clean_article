import '../../../../../_core_exports.dart';

class SecureStorageKeyParams {
  final SecureStorageKeys key;
  final SharedStorageObjectTypeState? sharedStorageObjectTypeState;

  SecureStorageKeyParams(
      {required this.key, this.sharedStorageObjectTypeState});
}
