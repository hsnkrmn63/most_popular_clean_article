import 'package:dartz/dartz.dart';

import '../../../../../_core_exports.dart';

abstract class SecureStorageRepository {
  Future<Either<Failure, String>> getSecureDataFromKey(
      final SecureStorageKeys key);
  Future<Either<Failure, void>> removeSecureDataFromKey(
      final SecureStorageKeys key);
  Future<Either<Failure, void>> saveSecureDataFromKey(
      final SecureStorageKeys key, final String data);
  Future<Either<Failure, dynamic>> getSharedStorageDataFromKey(
      final SecureStorageKeys key,
      SharedStorageObjectTypeState sharedStorageObjectTypeState);
  Future<Either<Failure, void>> removeSharedStorageSecureDataFromKey(
      final SecureStorageKeys key);
  Future<Either<Failure, void>> saveSharedStorageSecureDataFromKey(
      final SecureStorageKeys key,
      dynamic data,
      SharedStorageObjectTypeState sharedStorageObjectTypeState);
}
