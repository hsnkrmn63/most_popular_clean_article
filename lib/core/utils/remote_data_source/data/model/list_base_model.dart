import '../../../../_core_exports.dart';

class ListBaseModel<T> {
  ListBaseModel({
    required this.data,
  });

  final List<T> data;

  factory ListBaseModel.fromJson(final String str,
          final Function(Map<String, dynamic>)? mappingFunction) =>
      ListBaseModel.fromMap(json.decode(str), mappingFunction);

  String toJson() => json.encode(toMap());

  factory ListBaseModel.fromMap(final Map<String, dynamic> json,
      final Function(Map<String, dynamic>)? mappingFunction) {
    try {
      if (json["data"] != null && json["data"] != []) {
        final ListBaseModel<T> model = ListBaseModel<T>(
          data: mappingFunction != null
              ? List<T>.from(json["data"].map((final x) => mappingFunction(x)))
              : List<T>.from(json["data"].map((final x) => x)),
        );

        return model;
      } else {
        throw EmptyResultFailure();
      }
    } catch (e) {
      throw EmptyResultFailure();
    }
  }

  Map<String, dynamic> toMap() => {
        "data":
            List<T>.from(data.map((final x) => (x as BaseModelRepo).toMap())),
      };
}
