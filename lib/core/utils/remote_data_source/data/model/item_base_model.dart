import '../../../../_core_exports.dart';

class ItemBaseModel<T> {
  ItemBaseModel({
    required this.result,
  });

  final T result;

  factory ItemBaseModel.fromJson(final String str,
          final Function(Map<String, dynamic>)? mappingFunction) =>
      ItemBaseModel.fromMap(json.decode(str), mappingFunction);

  factory ItemBaseModel.fromMap(
    final Map<String, dynamic> json,
    final Function(Map<String, dynamic>)? mappingFunction,
  ) {
    try {
      final ItemBaseModel<T> model = ItemBaseModel<T>(
        result: mappingFunction != null ? mappingFunction(json) : json,
      );

      return model;
    } catch (e) {
      throw EmptyResultFailure();
    }
  }
}
