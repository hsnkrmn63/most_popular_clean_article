import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;

import '../../../../_core_exports.dart';

class BaseRequestRepoImpl implements IBaseRequestRepository {
  @override
  Future<Either<Failure, dynamic>> fetchNoNetwork(MainEndpoints path,
      {required HttpTypes type,
      dynamic data,
      Map<String, dynamic>? queryParameters,
      String? addGetPath,
      void Function(int p1, int p2)? onReceiveProgress}) async {
    final Map<String, String> headers = _getHeaders();
    http.Response response;
    try {
      Uri url = Uri.https(
          DomainResourceEnum.nyTimes.getUrlByState(),
          serviceLocator<RemoteEndpoints>().getEndpoint(path) +
              (addGetPath ?? ""),
          queryParameters);
      switch (type) {
        case HttpTypes.post:
          response =
              await http.post(url, headers: headers, body: jsonEncode(data));
          break;
        case HttpTypes.get:
          response = await http.get(url, headers: headers);
          break;
        case HttpTypes.delete:
          response =
              await http.delete(url, headers: headers, body: jsonEncode(data));
          break;
        case HttpTypes.put:
          response =
              await http.put(url, headers: headers, body: jsonEncode(data));
          break;
        case HttpTypes.patch:
          response =
              await http.patch(url, headers: headers, body: jsonEncode(data));
          break;
        case HttpTypes.multiPartFormData:
          response = await http.get(
            url,
            headers: headers,
          );
          break;
      }
    } on SocketException {
      return Left(NoInternetConnectionFailure());
    } on Failure catch (failure) {
      return Left(failure);
    }

    return _handleResponse(
      response,
      serviceLocator<RemoteEndpoints>().getEndpoint(path),
    );
  }

  ///
  ///*headers
  ///
  Map<String, String> _getHeaders() {
    final Map<String, String> headers = {};

    headers["content-type"] = "application/json;charset=UTF-8";
    headers["accept"] = "application/json, text/plain, */*";

    return headers;
  }

  Future<Either<Failure, dynamic>> _handleResponse(
      final http.Response response, String? apiPath) async {
    _printRequestAttributes(response, apiPath);
    StatusCodeEnums statusCode =
        StatusCodeEnums.statusCode200.statusCodeToEnum(response.statusCode);
    if (statusCode.isSuccess()) {
      return Right(response.body);
    } else if (statusCode == StatusCodeEnums.statusCode500) {
      return Left(Failure.getFailure(response.body));
    } else if (statusCode == StatusCodeEnums.statusCode502) {
      return Left(Failure.getFailure(
          "Beklemedik bir hata oluştu. Tekrar Deneyiniz..."));
    } else if (statusCode == StatusCodeEnums.statusCode400) {
      return Left(Failure.getFailure(response.body));
    } else {
      return Left(statusCode.stateToFailure()!);
    }
  }

  void _printRequestAttributes(
    final http.Response response,
    String? apiPath,
  ) {
    if (DebugModeOptions.printEndpointToConsole) {
      printWithoutLimits('Request On:');
      printWithoutLimits(
          'base Url:${DomainResourceEnum.nyTimes.getUrlByState()}$apiPath');
      printWithoutLimits('Ends With:');
      printWithoutLimits(response.statusCode);
    }
    if (DebugModeOptions.printRequestToConsole) {
      printWithoutLimits('-' * 200);
      printWithoutLimits('Request:');
      printWithoutLimits(response.body);
    }
    if (DebugModeOptions.printResponseToConsole) {
      printWithoutLimits('-' * 200);
      printWithoutLimits('Response:');
      printWithoutLimits(response.body);
    }
    printWithoutLimits('*/' * 100);
  }
}
