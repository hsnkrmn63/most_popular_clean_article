import '../../../../_core_exports.dart';

enum StatusCodeEnums {
  statusCode200, //* OK
  statusCode201, //* Created
  statusCode204, //* No Content
  statusCode400, //* Bad Request
  statusCode401, //* Unauthorized
  statusCode403, //* Forbidden
  statusCode404, //* Not Found
  statusCode500, //* Internal Exception
  statusCode502, //* Internal Exception
  statusCode504, //* Gateway TimeOut
}

extension StatusCodeEnumsExtension on StatusCodeEnums {
  StatusCodeEnums statusCodeToEnum(final int statusCode) {
    switch (statusCode) {
      case 200:
        return StatusCodeEnums.statusCode200;
      case 201:
        return StatusCodeEnums.statusCode201;
      case 204:
        return StatusCodeEnums.statusCode204;
      case 400:
        return StatusCodeEnums.statusCode400;
      case 401:
        return StatusCodeEnums.statusCode401;
      case 403:
        return StatusCodeEnums.statusCode403;
      case 404:
        return StatusCodeEnums.statusCode404;
      case 500:
        return StatusCodeEnums.statusCode500;
      case 502:
        return StatusCodeEnums.statusCode502;
      case 504:
        return StatusCodeEnums.statusCode504;
      default:
        return StatusCodeEnums.statusCode404;
    }
  }

  Failure? stateToFailure() {
    switch (this) {
      case StatusCodeEnums.statusCode400:
        return BadRequestFailure();
      case StatusCodeEnums.statusCode401:
        return UnauthorizedFailure();
      case StatusCodeEnums.statusCode403:
        return ForbiddenFailure();
      case StatusCodeEnums.statusCode404:
        return NotFoundFailure();
      case StatusCodeEnums.statusCode500:
        return InternalFailure();
      case StatusCodeEnums.statusCode504:
        return GatewayTimeOutFailure();
      case StatusCodeEnums.statusCode200:
        return null;
      case StatusCodeEnums.statusCode204:
        return null;
      case StatusCodeEnums.statusCode201:
        return null;
      case StatusCodeEnums.statusCode502:
        return GatewayTimeOutFailure();
    }
  }

  bool isSuccess() {
    switch (this) {
      case StatusCodeEnums.statusCode200:
        return true;
      case StatusCodeEnums.statusCode201:
        return true;
      case StatusCodeEnums.statusCode204:
        return true;
      case StatusCodeEnums.statusCode400:
        return false;
      case StatusCodeEnums.statusCode401:
        return false;
      case StatusCodeEnums.statusCode403:
        return false;
      case StatusCodeEnums.statusCode404:
        return false;
      case StatusCodeEnums.statusCode500:
        return false;
      case StatusCodeEnums.statusCode504:
        return false;
      case StatusCodeEnums.statusCode502:
        return false;
    }
  }

  bool isErrorState() {
    switch (this) {
      case StatusCodeEnums.statusCode200:
        return false;
      case StatusCodeEnums.statusCode201:
        return false;
      case StatusCodeEnums.statusCode204:
        return false;
      case StatusCodeEnums.statusCode400:
        return true;
      case StatusCodeEnums.statusCode401:
        return true;
      case StatusCodeEnums.statusCode403:
        return true;
      case StatusCodeEnums.statusCode404:
        return true;
      case StatusCodeEnums.statusCode500:
        return true;
      case StatusCodeEnums.statusCode504:
        return true;
      case StatusCodeEnums.statusCode502:
        return true;
    }
  }
}
