import 'package:flutter/foundation.dart';

import '../../../../_core_exports.dart';

enum ApplicationEnvironment { dev }

// return base url
extension ApplicationEnvironmentHelpers on ApplicationEnvironment {
  String getUrlState() {
    switch (this) {
      case ApplicationEnvironment.dev:
        return AppConstants.devApiUrl;
    }
  }
}

enum DomainResourceEnum {
  nyTimes,
}

extension DomainResourceEnumExtension on DomainResourceEnum {
  String getUrlByState() {
    switch (this) {
      case DomainResourceEnum.nyTimes:
        if (kDebugMode) {
          return serviceLocator<DebugModeOptions>()
              .applicationEnvironment
              .getUrlState();
        } else {
          return serviceLocator<DebugModeOptions>()
              .applicationEnvironment
              .getUrlState();
        }
    }
  }
}
