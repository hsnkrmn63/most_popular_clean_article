enum HttpTypes { post, get, delete, put, patch, multiPartFormData }

extension NetworkTypeExtension on HttpTypes? {
  String get rawValue {
    switch (this) {
      case HttpTypes.post:
        return 'POST';

      case HttpTypes.get:
        return 'GET';

      case HttpTypes.delete:
        return 'DELETE';

      case HttpTypes.put:
        return 'PUT';

      case HttpTypes.patch:
        return 'PATCH';

      case HttpTypes.multiPartFormData:
        return 'POST';

      default:
        throw 'ERROR TYPE';
    }
  }
}
