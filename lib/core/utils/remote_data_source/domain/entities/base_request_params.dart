import '../../../../_core_exports.dart';

class BaseRequestParams {
  final HttpTypes type;
  final MainEndpoints path;
  final dynamic data;
  final String? addGetPath;

  final Map<String, dynamic>? queryParameters;

  BaseRequestParams({
    required this.type,
    required this.path,
    this.data,
    this.addGetPath,
    this.queryParameters,
  });
}
