import '../../../../_core_exports.dart';

class RemoteEndpoints {
  Map<MainEndpoints, String> remoteEndpointList = {
    MainEndpoints.mostPopular: "/svc/mostpopular/v2/viewed/",
  };

  String getEndpoint(final MainEndpoints endPoint) {
    if (remoteEndpointList.containsKey(endPoint)) {
      return remoteEndpointList[endPoint]!;
    }
    throw NotFoundFailure();
  }

  void setRemoteEndpointList(final Map remoteEndpoints) {
    for (final endpoint in remoteEndpoints.entries) {
      try {
        final MainEndpoints mainEndpointKey = MainEndpoints.values
            .firstWhere((final element) => element.name == endpoint.key);
        remoteEndpointList[mainEndpointKey] = endpoint.value;
      } catch (error) {
        debugPrint(endpoint.toString());
      }
    }
  }
}
