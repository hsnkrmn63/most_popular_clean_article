import 'package:dartz/dartz.dart';

import '../../../../_core_exports.dart';

abstract class IBaseRequestRepository {
  Future<Either<Failure, dynamic>> fetchNoNetwork(MainEndpoints path,
      {required HttpTypes type,
      dynamic data,
      Map<String, dynamic>? queryParameters,
      String? addGetPath,
      void Function(int, int)? onReceiveProgress});
}
