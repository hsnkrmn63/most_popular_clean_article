import 'package:dartz/dartz.dart';

import '../../../../_core_exports.dart';

class BaseRequestUsecase implements Usecase<dynamic, BaseRequestParams> {
  late IBaseRequestRepository repository;
  BaseRequestUsecase(this.repository);

  @override
  Future<Either<Failure, dynamic>> call(BaseRequestParams params) {
    return repository.fetchNoNetwork(params.path,
        type: params.type,
        data: params.data,
        addGetPath: params.addGetPath,
        queryParameters: params.queryParameters);
  }
}
