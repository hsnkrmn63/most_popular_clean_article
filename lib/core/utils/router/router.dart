import 'package:most_popular_clean_article/feature/most_popular/data/models/most_popular.dart';
import 'package:most_popular_clean_article/feature/most_popular/view/_exports.dart';
import 'package:flutter/cupertino.dart';

import '../../_core_exports.dart';

Route<dynamic> generateRoute(final RouteSettings settings) {
  switch (settings.name) {
    case AppRouters.mostPopularView:
      return CupertinoPageRoute(builder: (final _) => const MostPopularView());
    case AppRouters.mostPopularDetailView:
      final args = settings.arguments as MostPopular;
      return CupertinoPageRoute(
          builder: (final _) => MostPopularDetailView(
                model: args,
              ));
    default:
      return MaterialPageRoute(builder: (final _) => const MostPopularView());
  }
}
