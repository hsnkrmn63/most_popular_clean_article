import 'package:most_popular_clean_article/feature/most_popular/view/most_popular_view.dart';
import 'core/_core_exports.dart';
import 'core/init/injection_container.dart' as dependency_injection;
import 'core/utils/router/router.dart' as router;
export 'package:flutter_localizations/flutter_localizations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dependency_injection.init();
  HttpOverrides.global = MyHttpOverrides();
  await lockScreenOrientation();
  serviceLocator.allReady().then((value) async {
    try {} catch (e) {
      debugPrint(e.toString());
    } finally {
      runApp(
        MultiProvider(
            providers: MainViewModels.getMainViewModels(),
            child: Consumer<ConnectionViewModel>(
              builder: (
                final BuildContext context,
                final ConnectionViewModel connectionViewModel,
                final Widget? widget,
              ) {
                return const MyApp();
              },
            )),
      );
    }
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(390, 812),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          return MaterialApp(
            title: 'Media Probe',
            debugShowCheckedModeBanner: false,
            navigatorKey: GlobalContextKey.instance.globalKey,
            navigatorObservers: [
              serviceLocator<GeneralAppViewModel>().routeObserver
            ],
            localizationsDelegates: const [
              AppStrings.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: const [
              Locale('tr', 'TR'),
              Locale('en', 'EN'),
            ],
            home: const MostPopularView(),
            onGenerateRoute: router.generateRoute,
          );
        });
  }
}
