import '../../../core/_core_exports.dart';
import '../../_exports.dart';

class MostPopularDetailView extends StatelessWidget {
  const MostPopularDetailView({super.key, required this.model});
  final MostPopular model;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.lightGrey,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: AppColors.turquoise,
        title: Text(
          AppStrings.of().detail!,
          style: context.size20Bold500,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ConstrainedBox(
              constraints:
                  BoxConstraints(maxHeight: 240.0.r, maxWidth: double.infinity),
              child: CachedNetworkImage(
                imageUrl: serviceLocator<MostPopularViewmModel>()
                    .getforDetailImageUrl(model),
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) =>
                    const CircularProgressIndicator(),
                errorWidget: (context, url, error) => Container(
                  color: AppColors.saltBlack,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 21.w),
              child: Column(
                children: [
                  SizedBox(
                    height: 21.h,
                  ),
                  Text(
                    model.title!,
                    textAlign: TextAlign.center,
                    style: context.size20Bold500
                        .copyWith(height: 1.5, color: AppColors.saltBlack),
                  ),
                  SizedBox(
                    height: 21.h,
                  ),
                  Text(
                    model.adxKeywords!,
                    textAlign: TextAlign.center,
                    style: context.size14Bold400
                        .copyWith(height: 1.5.r, color: AppColors.saltBlack),
                  ),
                  SizedBox(
                    height: 21.h,
                  ),
                  Text(
                    dateSimpleFormat(model.publishedDate!),
                    style: context.size14Bold400,
                    overflow: TextOverflow.ellipsis,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
