import '../../../../core/_core_exports.dart';
import '../../_exports.dart';

class MostPopularView extends StatefulWidget {
  const MostPopularView({Key? key}) : super(key: key);

  @override
  State<MostPopularView> createState() => _MostPopularViewState();
}

class _MostPopularViewState extends State<MostPopularView> {
  @override
  void initState() {
    serviceLocator<MostPopularViewmModel>().onInit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (final BuildContext context,
        final MostPopularViewmModel viewModel, final Widget? child) {
      return Scaffold(
        backgroundColor: AppColors.lightGrey,
        appBar: AppBar(
          backgroundColor: AppColors.turquoise,
          title: Text(
            AppStrings.of().nyTimesMostPopular!,
            style: context.size20Bold500,
          ),
        ),
        body: SafeArea(
          child: BasePageView(
              basePageState: viewModel.basePageState,
              successWidget: ListView.builder(
                itemCount: viewModel.getMostPopularsLength(),
                itemBuilder: (BuildContext context, int index) =>
                    MostPopularListTile(model: viewModel.getMostPopular(index)),
              ),
              refreshFunction: () => viewModel.refreshFunc()),
        ),
      );
    });
  }
}
