import 'package:most_popular_clean_article/feature/most_popular/data/models/most_popular.dart';
import 'package:most_popular_clean_article/feature/most_popular/viewmodel/most_popular_viewmodel.dart';

import '../../../core/_core_exports.dart';

class MostPopularListTile extends StatelessWidget {
  const MostPopularListTile({super.key, required this.model});
  final MostPopular model;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
      child: ListTile(
        onTap: () => serviceLocator<MostPopularViewmModel>().goToDetail(model),
        leading: SizedBox(
            height: double.infinity,
            child: ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 60.0.r, maxWidth: 60.r),
              child: CachedNetworkImage(
                imageUrl:
                    serviceLocator<MostPopularViewmModel>().getImageUrl(model),
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100.r),
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                placeholder: (context, url) =>
                    const CircularProgressIndicator(),
                errorWidget: (context, url, error) => Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: AppColors.saltBlack),
                ),
              ),
            )),
        trailing: const SizedBox(
            height: double.infinity, child: Icon(Icons.arrow_right_sharp)),
        title: Text(
          model.title!,
          textAlign: TextAlign.left,
          style: context.size14Bold600.copyWith(height: 1.5),
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
        ),
        subtitle: Column(
          children: [
            Text(
              model.title ?? "",
              style: context.size14Bold400.copyWith(
                height: 1.5,
              ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const Icon(
                  Icons.calendar_month,
                  color: AppColors.grey,
                ),
                SizedBox(
                  width: 2.w,
                ),
                Text(
                  dateSimpleFormat(model.publishedDate!),
                  style: context.size14Bold400,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
