import '../../../_exports.dart';

class MostPopulars {
  final String? status;
  final String? copyright;
  final int? numResults;
  late List<MostPopular>? results;

  MostPopulars({
    this.status,
    this.copyright,
    this.numResults,
    this.results,
  });

  factory MostPopulars.fromMap(Map<String, dynamic> json) => MostPopulars(
        status: json["status"],
        copyright: json["copyright"],
        numResults: json["num_results"],
        results: json["results"] == null
            ? []
            : List<MostPopular>.from(
                json["results"]!.map((x) => MostPopular.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "status": status,
        "copyright": copyright,
        "num_results": numResults,
        "results": results == null
            ? []
            : List<MostPopular>.from(results!.map((x) => x.toMap())),
      };
}
