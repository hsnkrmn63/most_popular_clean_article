class MediaMetaData {
  late String? url;
  late String? format;
  late int? height;
  late int? width;

  MediaMetaData({
    this.url,
    this.format,
    this.height,
    this.width,
  });

  factory MediaMetaData.fromMap(Map<String, dynamic> json) => MediaMetaData(
        url: json["url"],
        format: json["format"],
        height: json["height"],
        width: json["width"],
      );

  Map<String, dynamic> toMap() => {
        "url": url,
        "format": format,
        "height": height,
        "width": width,
      };
}
