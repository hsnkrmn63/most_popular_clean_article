import 'package:most_popular_clean_article/feature/_exports.dart';
import 'package:dartz/dartz.dart';

import '../../../../../core/_core_exports.dart';

class MostPopularsRepoImpl implements MostPopularRepository {
  final BaseRequestUsecase _baseRequestUsecase;
  MostPopularsRepoImpl(
    this._baseRequestUsecase,
  );
  @override
  Future<Either<Failure, MostPopulars>> getMostPopulars(
      MostPopularParam param) async {
    final either = await _baseRequestUsecase(BaseRequestParams(
        type: HttpTypes.get,
        path: MainEndpoints.mostPopular,
        addGetPath: "${param.period.toString()}.json",
        queryParameters: param.toMapQuery()));

    return either.fold(
      Left.new,
      (final jsonData) async {
        try {
          final ItemBaseModel<MostPopulars> baseModel =
              ItemBaseModel<MostPopulars>.fromJson(
            jsonData,
            MostPopulars.fromMap,
          );

          return Right(baseModel.result);
        } on Failure catch (failure) {
          return Left(failure);
        }
      },
    );
  }
}
