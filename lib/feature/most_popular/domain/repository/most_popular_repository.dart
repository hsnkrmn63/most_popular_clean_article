import 'package:most_popular_clean_article/feature/_exports.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/error/failures/failure.dart';

abstract class MostPopularRepository {
  Future<Either<Failure, MostPopulars>> getMostPopulars(MostPopularParam param);
}
