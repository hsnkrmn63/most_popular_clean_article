class MostPopularParam {
  MostPopularParam({required this.period, required this.simpleKey});
  final int period;
  final String simpleKey;

  Map<String, dynamic> toMapQuery() => {
        "api-key": simpleKey,
      };
}
