import 'package:most_popular_clean_article/feature/_exports.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/_core_exports.dart';

class MostPopularsUsecase implements Usecase<MostPopulars, MostPopularParam> {
  MostPopularRepository repository;
  MostPopularsUsecase(this.repository);
  @override
  Future<Either<Failure, MostPopulars>> call(final params) async {
    return repository.getMostPopulars(params);
  }
}
