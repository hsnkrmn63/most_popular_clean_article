import 'package:most_popular_clean_article/feature/_exports.dart';

import '../../../../core/_core_exports.dart';

class MostPopularViewmModel extends ChangeNotifier {
  MostPopularViewmModel(this._mostPopularsUsecase);
  final MostPopularsUsecase _mostPopularsUsecase;

  //base state
  BasePageState basePageState = BasePageState.succes;

  FutureOr<void> onInit() async {
    _mostPopulars = null;
    await _fetchMostPopulars();
  }

  late MostPopulars? _mostPopulars;
  MostPopulars? get mostPopulars => _mostPopulars;

  Future<void> _fetchMostPopulars() async {
    basePageState = BasePageState.loading;
    notifyListeners();

    final fetchEither = await _mostPopularsUsecase(
      MostPopularParam(period: 7, simpleKey: AppConstants.apiKey),
    );
    fetchEither.fold((final failure) {
      failure.snackBarMethod();

      basePageState = BasePageState.succes;
    }, (final data) {
      _mostPopulars = data;
      basePageState = BasePageState.succes;
      _mostPopulars!.results!
          .sort((a, b) => a.publishedDate!.compareTo(b.publishedDate!));

      notifyListeners();
    });
    notifyListeners();
  }

  Future<void> refreshFunc() async {
    _mostPopulars = null;
    await _fetchMostPopulars();
  }

  int getMostPopularsLength() => (_mostPopulars?.results ?? []).length;

  MostPopular getMostPopular(int index) => _mostPopulars!.results![index];

  String getImageUrl(MostPopular model) =>
      (model.media!).isEmpty || (model.media!.first.mediaMetadata!.isEmpty)
          ? ""
          : model.media!.first.mediaMetadata!.first.url!;

  String getforDetailImageUrl(MostPopular model) =>
      (model.media!).isEmpty || (model.media!.first.mediaMetadata!.isEmpty)
          ? ""
          : model.media!.first.mediaMetadata!.last.url!;

  void goToDetail(MostPopular model) =>
      Go.to.page(AppRouters.mostPopularDetailView, arguments: model);
}
