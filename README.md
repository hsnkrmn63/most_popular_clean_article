# most_popular_clean_article

A flutter project written with clean architecture.

## Getting Started

The application is a simple popular news listing and detail display application. Emphasis is placed on clean architecture rather than professional design.

It is made up of two layers.(core,feature)

A few resources to get you started if this is your first Flutter project:

## Core
* It was made for the continuity of the application under core and for common use at different points within the application. and also simple migration can be done for use in different projects.

* There are structures such as network, local storage, remote storage, location, utils, extensions, failure func, error func, router, router manager, contacts, global widgets, global functions.

## Feature

* In this layer, files were created according to the page structure.
modularly, the network layer, the viewmodel layer, the repository layer, the view layer were created separately for each module.


